#
# A pass line betting strategy, reading stdin for win or loss
# The first argument is the bet amount.
#
passlinebet=$1
awk -v bet="$passlinebet" '/win/ { print (bet+0) } /loss/ { print -1*(bet+0) }'
