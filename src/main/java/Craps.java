import java.util.HashMap;
import java.util.TreeMap;

public class Craps { 

   private static final TreeMap<Integer, Long> freq = new TreeMap<>();
   //private static final int[] SUMS = new int[]{2,3,4,5,6,7,8,9,10,11,12};
   private static long numRolls = 0;
   private static boolean printSummary = false;
   
   static {
       for(int sum=2; sum<=12; sum++){
           freq.put(sum, 0L);
       }
   }
   
   public static void main(String[] args) {
      if(args.length == 1){
          int gamesToPlay = Integer.parseInt(args[0]);
          playNGames(gamesToPlay);
      } else if (args.length == 2) {
          if("-s".equals(args[0])){
              printSummary = true;
          } else {
              System.err.println(args[0] + " invalid flag; -s is only flag available");
              System.exit(1);
          }
          int gamesToPlay = Integer.parseInt(args[1]);
          playNGames(gamesToPlay);
      } else {
          System.out.println("java -jar craps.jar N");
          System.out.println("   N - number of games to play (integer)");
      }
      if(printSummary) {
          System.err.println("\nRolls: " + numRolls);
          System.err.println("Roll\tOcc\tPct");
          for(Integer sumOfTwoDice: freq.keySet()){
              Long occurences = freq.get(sumOfTwoDice);
              double pct = 100*((double)occurences)/numRolls;
              System.err.printf("%d\t%d\t%4f%n", sumOfTwoDice, occurences, pct);
          }
      }
   }

   private static void playNGames(int n){
       while(n > 0){
           System.out.println(playOneGame());
           n--;
       }
   }
   
   private static String playOneGame() {
       StringBuilder sb = new StringBuilder();
       int die1, die2, roll;
       die1 = (int)(6.0*Math.random() + 1.0);
       die2 = (int)(6.0*Math.random() + 1.0);
       roll = die1 + die2;
       store(roll);
       
       if (roll == 2 || roll == 3 || roll == 12) {
           //System.out.println("Immediate loss with: " + roll);
           sb.append(roll)
           .append(" loss");
       }
       else if (roll == 7 || roll == 11) {
           //System.out.println("Immediate win with: " + roll);
           sb.append(roll)
           .append(" win");
       }
       else { 
           StringBuilder pb = new StringBuilder();
           int point = roll; // point: 4, 5, 6, 8, 9, or 10
           //System.out.println("Point: " + point);
           pb.append(point)
           .append(" ");
           while (true) {  // keep rolling
               roll = (int)(6.0*Math.random() + 1.0) +
                       (int)(6.0*Math.random() + 1.0);
               store(roll);
               //System.out.println("\nNew roll: " + roll);
               pb.append(roll).append(" ");
               if (roll == point) {
                   //System.out.println("Made point, won");
                   //sb.append("win ").append(pb.toString());
                   sb.append(pb.toString()).append("win");
                   break;  // break out of loop, a win
               }
               if (roll == 7) {
                   //System.out.println("Lost with 7");
                   //sb.append("loss ").append(pb.toString());
                   sb.append(pb.toString()).append("loss");
                   break;  // break out of loop, a loss
               }
               else {
                   //System.out.println("No help");
               }
           }
       }
       //System.out.println(sb.toString());
       return sb.toString();
   }

   private static void store(int roll) {
       numRolls++;
       long existingCount = 0;
       if(freq.containsKey(roll)){
           existingCount = freq.get(roll);
       }
       existingCount++;
       freq.put(roll, existingCount);
   }
}