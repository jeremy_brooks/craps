Plays the game of craps.

`$ java -jar build/libs/craps.jar`

java -jar craps.jar N

   N - number of games to play (integer)


# Play a game

`$ java -jar build/libs/craps.jar 1`

7 win


# Play 5 games

`$ java -jar build/libs/craps.jar 5`

4 6 7 loss

6 6 win

3 loss

9 3 8 9 win

6 8 4 7 loss


# Play 10,000 games and see how many we win.

`$ java -jar build/libs/craps.jar 10000|grep win|wc -l`

4896


# Play some games and print only the summary

`$ java -jar build/libs/craps.jar -s 500 > /dev/nul`

Rolls: 1716

Roll    Occurences      Percentage

   2            44      2.564103

   3            98      5.710956

   4           129      7.517483

   5           209      12.179487

   6           240      13.986014

   7           273      15.909091

   8           221      12.878788

   9           202      11.771562

  10           161      9.382284

  11            97      5.652681

  12            42      2.447552


# Play 10 games and place $5 pass line bet for each game

`$ java -jar build/libs/craps.jar 10|./bet.sh 5`

-5

5

5

-5

-5

5

5

-5

5

5


# Play 10 games, $5 pass line bet, starting balance $20
Note: The balance.sh script exits once the balance is zero. 
For example, we only got to play 8 games of the 10 total played below.

`$ java -jar build/libs/craps.jar 10|./bet.sh 5|./balance.sh 20`

-5      15

-5      10

5       15

5       20

-5      15

-5      10

-5      5

-5      0
