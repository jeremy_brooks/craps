#
# Prints current balance, reading stdin for adjustments (+ or -)
# The first argument is the starting balance or bankroll.
# As soon as the balance is less than the bet amount, the script exits.
#
bankroll=$1
awk -v bal="$bankroll" 'BEGIN{sum=bal+0;} { w=($0+0); bet=(w>0)?w:-w; sum+=w; print w "\t" sum; if(sum < bet) { exit 1 } }'